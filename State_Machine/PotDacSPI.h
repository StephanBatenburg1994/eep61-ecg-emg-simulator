/*
 * PotDacSPI.h
 *
 *  Created on: 16 mei 2018
 *      Author: Cas
 *
 *
 *  CS1: DAC1  - P5.7   ---- DACECG output
 *  CS2: DAC2  - P2.1   ---- DACNoise output
 *  CS3: DAC0  - P5.2   ---- DAC50Hz output
 *  CS4: DP3   - P2.4   ---- noise regelen
 *  CS5: DP4   - P2.5   ---- 50 hz brom regelen
 *  CS6: DP1/2 - P2.7   ---- VrefRegelen DAC2/DAC1
 *  CS7: DP0   - P2.6   ---- VrefRegelen DAC ECG-EMG
 *
 *  PW0 - PB0 Feedback
 *  PW1 - PB1 Spanningsdeler
 *
 *                 MSP432P401
 *              -----------------
 *             |                 |
 *             |                 |
 *             |                 |
 *             |             P1.6|-> Data Out (UCB0SIMO)
 *             |                 |
 *             |             P1.7|<- Data In (UCB0SOMI)
 *             |                 |
 *             |             P1.5|-> Serial Clock Out (UCB0CLK)
 *
 *
 *
 */

#ifndef POTDACSPI_H_
#define POTDACSPI_H_

typedef enum POT
{
    VREF_POTDAC_ECG_EMG, VREF_POTDAC_NOISE, VREF_POTDAC_50HZ, POT_Feedback50HZ, POT_Spanningsdeler50HZ, POT_FeedbackNoise, POT_SpanningsdelerNoise
}POT;

typedef enum DAC
{
    DAC_ECG_EMG, DAC_Noise, DAC_50Hz
}DAC;

typedef enum SIGNAL
{
    ECG, EMG, NOISE, FIFTYHz
}signal_T;

void initSPI(void);
void SPI_Transmit_Pot(uint8_t potnumber, uint8_t value);
void SPI_Transmit_DAC(uint8_t DACnumber, uint16_t value);
uint8_t VppPOT_conversion(signal_T signal, float voltage);


#endif /* POTDACSPI_H_ */
