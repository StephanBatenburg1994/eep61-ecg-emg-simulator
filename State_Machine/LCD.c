#include "LCD.h"

/* I2C Master Configuration Parameter */
const eUSCI_I2C_MasterConfig i2cConfig =
{
        EUSCI_B_I2C_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
        48000000,                               // SMCLK = 48MHz
        EUSCI_B_I2C_SET_DATA_RATE_100KBPS,      // Desired I2C Clock of 100khz
        0,                                      // No byte counter threshold
        EUSCI_B_I2C_NO_AUTO_STOP                // No Autostop
};

void _i2c_init(void)
{
    /* Select Port 1 for I2C - Set Pin 6, 7 to input Primary Module Function,
         *   (UCB0SIMO/UCB0SDA, UCB0SOMI/UCB0SCL).
         */
        MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P6,
                GPIO_PIN4 + GPIO_PIN5, GPIO_PRIMARY_MODULE_FUNCTION);

        //![Simple I2C Example]
        /* Initializing I2C Master to SMCLK at 400kbs with no autostop */
        MAP_I2C_initMaster(EUSCI_B1_BASE, &i2cConfig);

        /* Specify slave address */
        MAP_I2C_setSlaveAddress(EUSCI_B1_BASE, 0x3c);

        /* Set Master in transmit mode */
        MAP_I2C_setMode(EUSCI_B1_BASE, EUSCI_B_I2C_TRANSMIT_MODE);

        /* Enable I2C Module to start operations */
        MAP_I2C_enableModule(EUSCI_B1_BASE);

        /* Enable and clear the interrupt flag */
        MAP_I2C_clearInterruptFlag(EUSCI_B1_BASE, EUSCI_B_I2C_NAK_INTERRUPT);

        /* Enable master transmit interrupt */
        MAP_I2C_enableInterrupt(EUSCI_B1_BASE, EUSCI_B_I2C_NAK_INTERRUPT);
        MAP_Interrupt_enableInterrupt(INT_EUSCIB1);
        //![Simple I2C Example]

    return;
}

void _lcd_startUp_screen(void)
{
    char * arrow = ">";
    _lcd_setCursor(0,0);
    _lcd_write(arrow,1);

    char * ecg = "ECG    3.30V 60 BPM";
    _lcd_setCursor(0,1);
    _lcd_write(ecg,19);

    char * emg = "EMG";
    _lcd_setCursor(1,1);
    _lcd_write(emg,3);

    char * Deactivated = "-";
    _lcd_setCursor(1,8);
    _lcd_write(Deactivated,1);

    char * fifty_Hz = "50 Hz  3.30V";
    _lcd_setCursor(2,1);
    _lcd_write(fifty_Hz, 12);


    char * noise = "NOISE  3.30V";
    _lcd_setCursor(3,1);
    _lcd_write(noise,12);
}

void _lcd_toggleArrow(void)
{
    static uint8_t row = 0;

    char * space = " ";
    _lcd_setCursor(row,0);
    _lcd_write(space, 1);

    row++;

    if(row > 3)
        row = 0;

    char * arrow = ">";
    _lcd_setCursor(row,0);
    _lcd_write(arrow, 1);

    return;
}

void _lcd_setBPM(uint8_t BPM)
{
    char BPM_string[6];
    sprintf(BPM_string, "%d BPM", BPM);

    _lcd_setCursor(0, 14);
    _lcd_write(BPM_string, 6);

    return;
}

void _lcd_setVpp(uint8_t signal, float voltage)
{
    char Volt_pp[5];
    _lcd_setCursor(signal, 8);
    if(signal > 3 || voltage < 0 || voltage > 5)
        return;

    sprintf(Volt_pp, "%.2fV", voltage);
    _lcd_write(Volt_pp, 5);

    return;
}

void _lcd_toggleSignal(void)
{
    static uint8_t signal = 0;
    signal++;

    if(signal > 1)
        signal = 0;

    switch(signal)
    {
    case 0:
        _lcd_clearLine(1);
        _lcd_setVpp(0, 3.30);
        _lcd_setBPM(60);
    break;

    case 1:
        _lcd_clearLine(0);
        _lcd_setVpp(1, 3.30);
        break;
    }

    return;
}

void _lcd_clearLine(uint8_t line)
{
    char * empty_line = "-           ";
    _lcd_setCursor(line,8);
    _lcd_write(empty_line,12);
    return;
}

void _lcd_init(void)
{
    uint16_t i;

    /* Set GPIO's 2.4 & 2.5 as outputs */
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN4);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN5);

    /* LCD screen hardware reset */
    GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN4);
    for(i = 0; i < 50000; i++);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN4);
    for(i = 0; i < 50000; i++);
    GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN4);

    _lcd_sendCommand(CLEAR_DISP_CMD);        //0x01

    _lcd_sendCommand(FUNC_SET_TBL0);         //0x39
    _lcd_sendCommand(BS1_1_CMD);             //0x1E
    _lcd_sendCommand(POWERDOWN_DIS_CMD);     //0x02
    _lcd_sendCommand(SEG_NORMAL_CMD);        //0x05
    _lcd_sendCommand(NW_CMD);                //0x09

    _lcd_sendCommand(FUNC_SET_TBL1);         //0x3a
    _lcd_sendCommand(DISP_ON_CURSBLINK_CMD); //0x0F
    _lcd_sendCommand(BS0_1_CMD);             //0x1C
    _lcd_sendCommand(INTERNAL_DRIVER_CMD);   //0x13
    _lcd_sendCommand(CONTRAST_CMD);          //0x79
    _lcd_sendCommand(POWER_ICON_CMD);        //0x5c
    _lcd_sendCommand(FOLLOW_CONTROL_CMD);    //0x6E

    _lcd_sendCommand(FUNC_SET_TBL2);         //0x38

    return;
}

void _lcd_sendCommand(uint8_t command)
{
    /* Check if I2C bus is busy */
    while (I2C_masterIsStopSent(EUSCI_B1_BASE) == EUSCI_B_I2C_SENDING_STOP);

    /* Send command register + command */
    I2C_masterSendMultiByteStart(EUSCI_B1_BASE, DISP_CMD);
    I2C_masterSendMultiByteFinish(EUSCI_B1_BASE, command);
}

void _lcd_setCursor(uint8_t line, uint8_t column)
{
    if(line > 3 || column > 19)
        return;

    _lcd_sendCommand(SET_DDRAM_CMD | (line*0x20 + column));
}

void _lcd_write(char * TxData, uint8_t length)
{
    uint8_t byteCtr;
    I2C_masterSendMultiByteStart(EUSCI_B1_BASE, RAM_WRITE_CMD);

    for(byteCtr = 0; byteCtr < length; byteCtr++)
    {
        I2C_masterSendMultiByteNext(EUSCI_B1_BASE, TxData[byteCtr]);
    }

    I2C_masterSendMultiByteStop(EUSCI_B1_BASE);

    return;
}

void _lcd_Contrast(uint8_t contrast)
{
    _lcd_sendCommand(FUNC_SET_TBL1);
    _lcd_sendCommand(0x70 | (contrast & 0x70));
    _lcd_sendCommand(POWER_ICON_CMD | ((contrast >> 4) & 0x03));
    _lcd_sendCommand(FUNC_SET_TBL2);
}

void _lcd_CursorDisplay(uint8_t show, uint8_t blink)
{
    _lcd_sendCommand(FUNC_SET_TBL1);
    _lcd_sendCommand(0x0C | ((show)?0x02:0x00) | ((blink)?0x01:0x00));
    _lcd_sendCommand(FUNC_SET_TBL2);
}
