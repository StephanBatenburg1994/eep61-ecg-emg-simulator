/*
 *  File: setup.h
 *  Author @ Stephan Batenburg
 *  Date: May 21, 2018
 *
 *  Description:
 *  This library is used for setting up the variables that are used for the ECG/EMG and noise outputs.
 */

#ifndef __SETUP__

/* DriverLib Includes */
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>

#define MAIN_CLOCKSPEED 48000000
#define SEC_CLOCKSPEED 32000


//Use these to set the kind of output you want for the 50Hz output
typedef enum Mains_Type{
    MAINS_TRIANGLE, MAINS_SINE, MAINS_SQUARE
}Mains_Type;

//Use these to set ECG or EMG output
typedef enum Output_Type{
    ECG_SIGNAL, EMG_SIGNAL
}Output_Type;

typedef struct{
    bool Noise_On;
    bool Mains_On;
    Mains_Type Select_Mains_SignalType;
    uint16_t Mains_Amplitude;
    uint16_t Noise_Amplitude;
    uint16_t Current_Noise_Value;
    uint16_t Current_Mains_sample;
} noise_Settings;

typedef struct{
    Output_Type Select_EMG_ECG;
    uint16_t Amplitude_EMG;
    uint16_t Amplitude_ECG;
    uint8_t Heartbeat_ECG;
    uint16_t Current_ECG_Value;
    uint16_t Current_EMG_Value;
} ECG_EMG_Settings;


//Sets default settings for noise and ECG/EMG
void output_Set_Default_Settings(noise_Settings * NoiseSettings, ECG_EMG_Settings * ECGEMGSettings);

//set the interrupt for the system tick timing
void set_Timers(void);

//set interrupt for the buttons on the PCB
void Setup_PinInterrupts(void);

#endif // __SETUP__
