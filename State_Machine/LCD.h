/*
 * LCD.h
 *
 *  Created on: Jun 21, 2018
 *      Author: caspar
 */

#ifndef LCD_H_
#define LCD_H_

#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>

/* Slave Address for I2C Slave */
#define SLAVE_ADDR 0x3C

/*LCD COMMAND SET*/
#define DISP_CMD                0x00  // Command for writing commands to display
#define RAM_WRITE_CMD           0x40 // Write to display RAM
#define CLEAR_DISP_CMD          0x01 // Clear display command
#define HOME_CMD                0x02 // Set cursors at home (0,0)
#define DISP_ON_CMD             0x0C // Display on command
#define DISP_OFF_CMD            0x08 // Display off Command
#define SET_DDRAM_CMD           0x80 // Set DDRAM address command
#define CONTRAST_CMD            0x79 // Set contrast LCD command
#define BS1_1_CMD               0x1E
#define POWERDOWN_DIS_CMD       0x02
#define SEG_NORMAL_CMD          0x05
#define NW_CMD                  0x09
#define DISP_ON_CURSBLINK_CMD   0x0F
#define BS0_1_CMD               0x1C
#define INTERNAL_DRIVER_CMD     0x13
#define POWER_ICON_CMD          0x5C
#define FOLLOW_CONTROL_CMD      0x6E
#define FUNC_SET_TBL2           0x38
#define FUNC_SET_TBL0           0x3A // Function set - 8 bit, 2 line display 5x8, inst table 0
#define FUNC_SET_TBL1           0x39 // Function set - 8 bit, 2 line display 5x8, inst table 1

#define FIFTY_Hz                2
#define NOISE                   3
#define EMG                     1
#define ECG                     0

/* function prototypes */

void _i2c_init(void);
void _lcd_init(void);

void _lcd_sendCommand(uint8_t command);
void _lcd_write(char * TxData, uint8_t length);

void _lcd_setCursor(uint8_t line, uint8_t column);
void _lcd_Contrast(uint8_t contrast);
void _lcd_CursorDisplay(uint8_t show, uint8_t blink);
void _lcd_startUp_screen(void);
void _lcd_toggleArrow(void);
void _lcd_setBPM(uint8_t BPM);
void _lcd_setVpp(uint8_t signal, float voltage);
void _lcd_toggleSignal(void);
void _lcd_clearLine(uint8_t line);


#endif /* LCD_H_ */
