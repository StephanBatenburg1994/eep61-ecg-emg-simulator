/*
 * File: main.c
 * @ Author: Stephan Batenburg
 * Date: may 30, 2018
 */

/* DriverLib Includes */
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

/* Standard Includes */
#include <stdint.h>
#include <stdbool.h>

//includes for project
#include "setup.h"
#include "states.h"
#include "PotDacSPI.h"

uint8_t timer = 0;

int main(void)
{
    //test

    /* Stop Watchdog  */
    WDT_A_holdTimer();

    //enable SPI for DACs and POTs
    initSPI();

    //set all variables to default values
    noise_Settings NoiseSettings;
    ECG_EMG_Settings ECGEMG_Settings;

    output_Set_Default_Settings(&NoiseSettings, &ECGEMG_Settings);

    //these are the signals that will be displayed
    uint16_t ECG_Samples[] = {939, 940, 941, 942, 944, 945, 946, 947, 951, 956, 962, 967, 973, 978, 983, 989, 994, 1000, 1005, 1015, 1024, 1034, 1043, 1053, 1062, 1075, 1087, 1100, 1112, 1121,1126, 1131, 1136, 1141, 1146, 1151, 1156, 1164, 1172, 1179, 1187, 1194, 1202, 1209, 1216, 1222, 1229, 1235, 1241, 1248, 1254, 1260, 1264, 1268, 1271, 1275, 1279, 1283, 1287, 1286, 1284, 1281, 1279, 1276, 1274, 1271, 1268, 1266, 1263, 1261, 1258, 1256, 1253, 1251, 1246, 1242, 1237, 1232, 1227, 1222, 1218, 1215, 1211, 1207, 1203, 1199, 1195, 1191, 1184, 1178, 1171, 1165, 1159, 1152, 1146, 1141, 1136, 1130, 1125, 1120, 1115, 1110, 1103, 1096, 1088, 1080, 1073, 1065, 1057, 1049, 1040, 1030, 1021, 1012, 1004, 995, 987, 982, 978, 974, 970, 966, 963, 959, 955, 952, 949, 945, 942, 939, 938, 939, 940, 941, 943, 944, 945, 946, 946, 946, 946, 946, 946, 946, 946, 947, 950, 952, 954, 956, 958, 960, 962, 964, 965, 965, 965, 965, 965, 965, 963, 960, 957, 954, 951, 947, 944, 941, 938, 932, 926, 920, 913, 907, 901, 894, 885, 865, 820, 733, 606, 555, 507, 632, 697, 752, 807, 896, 977, 1023, 1069, 1127, 1237, 1347, 1457, 2085, 2246, 2474, 2549, 2595, 2641, 2695, 3083, 3135, 3187, 3217, 3315, 3403, 3492, 3581, 3804, 3847, 3890, 3798, 3443, 3453, 3297, 3053, 2819, 2810, 2225, 2258, 1892, 1734, 1625, 998, 903, 355, 376, 203, 30, 33, 61, 90, 119, 160, 238, 275, 292, 309, 325, 343, 371, 399, 429, 484, 542, 602, 652, 703, 558, 802, 838, 856, 875, 895, 917, 938, 967, 1016, 1035, 1041, 1047, 1054, 1060, 1066, 1066, 1064, 1061, 1058, 1056, 1053, 1051, 1048, 1046, 1043, 1041, 1038, 1035, 1033, 1030, 1028, 1025, 1022, 1019, 1017, 1014, 1011, 1008, 1006, 1003, 1001, 999, 998, 996, 994, 993, 991, 990, 988, 986, 985, 983, 981, 978, 976, 973, 971, 968, 966, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 974, 976, 978, 980, 983, 985, 987, 989, 991, 993, 995, 997, 999, 1002, 1006, 1011, 1015, 1019, 1023, 1028, 1032, 1036, 1040, 1045, 1050, 1055, 1059, 1064, 1069, 1076, 1082, 1088, 1095, 1101, 1107, 1114, 1120, 1126, 1132, 1141, 1149, 1158, 1166, 1173, 1178, 1183, 1188, 1193, 1198, 1203, 1208, 1214, 1221, 1227, 1233, 1240, 1246, 1250, 1254, 1259, 1263, 1269, 1278, 1286, 1294, 1303, 1309, 1315, 1322, 1328, 1334, 1341, 1343, 1345, 1347, 1349, 1351, 1353, 1355, 1357, 1359, 1359, 1359, 1359, 1359, 1358, 1356, 1354, 1352, 1350, 1347, 1345, 1343, 1341, 1339, 1336, 1334, 1332, 1329, 1327, 1324, 1322, 1320, 1317, 1315, 1312, 1307, 1301, 1294, 1288, 1281, 1275, 1270, 1265, 1260, 1256, 1251, 1246, 1240, 1233, 1227, 1221, 1214, 1208, 1201, 1194, 1186, 1178, 1170, 1162, 1154, 1148, 1144, 1140, 1136, 1131, 1127, 1123, 1118, 1114, 1107, 1099, 1090, 1082, 1074, 1069, 1064, 1058, 1053, 1048, 1043, 1038, 1034, 1029, 1025, 1021, 1017, 1013, 1009, 1005, 1001, 997, 994, 990, 991, 992, 994, 996, 997, 999, 998, 997, 996, 995, 994, 993, 991, 990, 989, 989, 989, 989, 989, 989, 989, 988, 986, 984, 983, 981, 980, 982, 984, 986, 988, 990, 993, 995, 997, 999, 1002, 1005, 1008, 1012};
    uint16_t noise_Samples[] = {1391, 2009, 2027, 2499, 1278, 1758, 2074, 1859, 2712, 1753, 954, 2192, 1340, 2426, 2062, 1762, 1508, 2643, 2024, 1704, 1622, 1828, 2403, 2664, 1911, 2071, 2029, 1240, 1956, 1701, 1729, 1840, 744, 1606, 2202, 1948, 1694, 1764, 1468, 1621, 1667, 1826, 2135, 1892, 1868, 1566, 2291, 1994, 2126, 1988, 2223, 2505, 1727, 2117, 2172, 1896, 2665, 1509, 1706, 1931, 1910, 1811, 1708, 2230, 1876, 1577, 2585, 2265, 2496, 1889, 2666, 2268, 1177, 1394, 2133, 1854, 1842, 2330, 2581, 2056, 1951, 2800, 1896, 1720, 1662, 1606, 1933, 1556, 1157, 1386, 2074, 2337, 2381, 2825, 1466, 2446, 1933, 1713, 1729, 2256, 2160, 1178, 2310, 2230, 1805, 1507, 1592, 1406, 1880, 1745, 1775, 1989, 1781, 1221, 1661, 1691, 1214, 2254, 1033, 2069, 1782, 1754, 2159, 1779, 1809, 1551, 2350, 2297, 2175, 2113, 2191, 2178, 2058, 2153, 1616, 1912, 2120, 1610, 1900, 1825, 2397, 2089, 2338, 2149, 2304, 1862, 1946, 1649, 2055, 3130, 1939, 1882, 2243, 1275, 2351, 1561, 2127, 2011, 1873, 1613, 2067, 2350, 2173, 2458, 1345, 1908, 1360, 1946, 2117, 1593, 2413, 1710, 2149, 1761, 1597, 1605, 1320, 2508, 921, 1384, 1106, 1363, 783, 2141, 1916, 1488, 1380, 1373, 1789, 1091, 2063, 1864, 1763, 2161, 2038, 1762, 1912, 1847, 1650, 2345, 2381, 1929, 1489, 1431, 2000, 1794, 1575, 1953, 1426, 2036, 1263, 918, 1817, 1507, 2512, 2449, 1655, 1513, 1813, 1774, 2454, 1761, 2382, 2395, 2908, 1748, 1710, 2570, 1605, 1174, 1935, 2100, 1755, 1760, 1355, 2589, 1981, 1681, 2061, 2234, 1481, 1990, 1707, 2560, 1964, 1450, 1856, 2080, 2740, 1916, 2129, 2380, 2104, 1552, 1836, 1637, 2183, 2516, 1891, 2322, 1621, 1964, 1829, 2552, 2018, 2207, 1764, 1647, 2058, 1573, 1355, 1779, 2540, 1743, 1988, 1663, 1080, 1900, 2577, 1767, 1440, 1504, 2046, 1732, 2293, 1790, 1066, 1724, 2185, 2311, 2414, 2300, 2329, 2658, 2514, 1970, 1710, 1788, 2181, 2383, 1801, 2505, 1610, 1714, 1745, 1635, 2248, 1825, 2641, 1898, 2647, 928, 2045, 2013, 2674, 2005, 2612, 2657, 1652, 1424, 2243, 1574, 1837, 1672, 1940, 2139, 2185, 1954, 1521, 1561, 2017, 1026, 1430, 1274, 1769, 2094, 2846, 1898, 1473, 1719, 2083, 2309, 1716, 1898, 2374, 1989, 1932, 1768, 1746, 2323, 1687, 1881, 1486, 2141, 2000, 2077, 1889, 1603, 1866, 1386, 1765, 2823, 2569, 2247, 2075, 2231, 2056, 2273, 2382, 1491, 1824, 1889, 1678, 1502, 2409, 2167, 1536, 1479, 2071, 1624, 1421, 1801, 1562, 2240, 2823, 1220, 2235, 2069, 1563, 2285, 1545, 1147, 1451, 2131, 2064, 2301, 2096, 2404, 1098, 1778, 1755, 1877, 2327, 1629, 2001, 1136, 1534, 2276, 1203, 2466, 1706, 2111, 1429, 2202, 1899, 2402, 1681, 1606, 1598, 2690, 1768, 1256, 2279, 1910, 1758, 2107, 2255, 1645, 1944, 1776, 1669, 2090, 2189, 1352, 2344, 1960, 1806, 1265, 1797, 1872, 1942, 1725, 2270, 2527, 2397, 1808, 2034, 2063, 1651, 1895, 1664, 1928, 2544, 1825, 1608, 2451, 2249, 1690, 1605, 2464, 1913, 1974, 2040, 1648, 2336, 2841, 905, 1979, 2278, 1582, 1936, 1968, 1660, 2183, 2386, 1989, 2048, 1681, 2347, 1864, 2076, 2370, 1781, 1822, 1936, 1811, 1392, 2093, 1323, 1778, 1337, 2421, 1934, 2092, 1795, 2535, 1738, 2361, 2361, 1690, 2072, 1951, 2746, 1502, 1266, 2024, 1512, 1664, 2033, 1381, 2329, 2069};
    uint16_t mains_Samples[] = {2048, 3229, 3977, 4019, 3339, 2186, 983, 170, 44, 653, 1772, 2992, 3867, 4075, 3541, 2460, 1228, 297, 6, 463, 1501, 2739, 3724, 4095, 3717, 2727, 1489, 455, 5, 303, 1240, 2473, 3550, 4077, 3861, 2981, 1760, 643, 42, 175, 994, 2199, 3349, 4022, 3973, 3218, 2035, 857, 115, 81, 767, 1922, 3124, 3931, 4049, 3434, 2312, 1092, 223, 22, 563, 1648, 2879, 3806, 4089, 3625, 2583, 1345, 365, 1, 387, 1381, 2619, 3648, 4092, 3786, 2844, 1611, 537, 17, 241, 1126, 2349, 3462, 4057, 3916, 3091, 1885, 738, 70, 128, 888, 2073, 3249, 3985, 4012, 3319, 2161, 962, 160, 50, 671, 1797, 3015, 3878, 4072, 3524, 2436, 1205, 284, 8, 479, 1525, 2763, 3738, 4095, 3702, 2703, 1465, 440, 4, 316, 1263, 2497, 3567, 4080, 3849, 2959, 1735, 625, 37, 185, 1015, 2224, 3368, 4029, 3964, 3198, 2010, 837, 107, 88, 787, 1947, 3145, 3941, 4044, 3416, 2287, 1070, 212, 26, 581, 1673, 2902, 3819, 4087, 3608, 2559, 1322, 351, 1, 402, 1405, 2643, 3664, 4093, 3773, 2821, 1586, 521, 14, 253, 1149, 2374, 3480, 4061, 3906, 3070, 1859, 718, 64, 136, 909, 2098, 3269, 3993, 4005, 3300, 2136, 940, 150, 55, 690, 1822, 3037, 3890, 4068, 3507, 2411, 1183, 271, 10, 496, 1550, 2786, 3752, 4094, 3687, 2679, 1441, 424, 3, 330, 1286, 2522, 3584, 4083, 3837, 2936, 1710, 607, 32, 196, 1037, 2249, 3387, 4035, 3955, 3177, 1985, 816, 99, 95, 806, 1973, 3166, 3950, 4038, 3397, 2262, 1048, 201, 30, 598, 1697, 2925, 3831, 4084, 3592, 2534, 1298, 337, 2, 417, 1429, 2667, 3679, 4094, 3759, 2798, 1562, 504, 12, 265, 1171, 2399, 3498, 4066, 3895, 3048, 1834, 699, 58, 146, 930, 2123, 3290, 4001, 3997, 3280, 2111, 919, 141, 61, 709, 1847, 3059, 3900, 4064, 3489, 2386, 1160, 259, 13, 512, 1574, 2810, 3766, 4093, 3672, 2655, 1417, 409, 2, 344, 1310, 2546, 3600, 4086, 3825, 2913, 1685, 589, 28, 206, 1059, 2274, 3406, 4041, 3946, 3156, 1960, 796, 91, 103, 827, 1998, 3187, 3960, 4032, 3378, 2237, 1026, 190, 35, 616, 1722, 2947, 3843, 4082, 3575, 2510, 1275, 323, 3, 432, 1453, 2691, 3694, 4095, 3745, 2774, 1537, 488, 9, 277, 1194, 2423, 3515, 4070, 3884, 3026, 1809, 680, 52, 155, 951, 2149, 3309, 4008, 3989, 3259, 2086, 898, 132, 67, 728, 1872, 3081, 3911, 4059, 3471, 2361, 1137, 247, 16, 529, 1599, 2833, 3780, 4092, 3656, 2631, 1393, 394, 1, 358, 1333, 2571, 3617, 4088, 3812, 2891, 1660, 572, 24, 218, 1081, 2299, 3425, 4046, 3936, 3134, 1935, 777, 84, 111, 847, 2023, 3208, 3968, 4026, 3358, 2211, 1005, 180, 39, 634, 1747, 2970, 3855, 4079, 3559, 2485, 1252, 310, 4, 448, 1477, 2715, 3709, 4095, 3731, 2751, 1513, 471, 7, 290, 1217, 2448, 3533, 4074, 3873, 3004, 1784, 662, 47, 165, 972, 2174, 3329, 4015, 3981, 3239, 2061, 878, 123, 74, 747, 1897, 3102, 3921, 4054, 3453, 2336, 1115, 235, 19, 546, 1623, 2856, 3793, 4091, 3641, 2607, 1369, 379, 1, 372, 1357, 2595, 3633, 4090, 3799, 2868, 1636, 555, 21, 229, 1104, 2324, 3443, 4052, 3926, 3113, 1910, 757, 77, 119, 867, 2048};
    uint16_t EMG_Samples[] = {0,0};

    //set up timer interrupts
    set_Timers();

    Setup_PinInterrupts();

    while(1)
    {
        switch(Current_State)
        {
        case STATE_SETUP:
            SysTick_disableInterrupt();
            SPI_Transmit_Pot(VREF_POTDAC_50HZ, 127);
            SPI_Transmit_Pot(VREF_POTDAC_ECG_EMG, 127);
            SPI_Transmit_Pot(VREF_POTDAC_NOISE, 127);
            SPI_Transmit_Pot(POT_Feedback50HZ, 127);
            SPI_Transmit_Pot(POT_FeedbackNoise, 127);
            SPI_Transmit_Pot(POT_Spanningsdeler50HZ, 127);
            SPI_Transmit_Pot(POT_SpanningsdelerNoise, 127);
            SysTick_enableInterrupt();

            Current_State = STATE_ECG;
            break;

        case STATE_ECG:

            if(timer >=1)
            {
                SysTick_disableInterrupt();
                //Play ECG signal

                SPI_Transmit_DAC(DAC_ECG_EMG, ECG_Samples[ECGEMG_Settings.Current_ECG_Value]);

                ECGEMG_Settings.Current_ECG_Value++;
                if(ECGEMG_Settings.Current_ECG_Value > AMOUNT_ECG_SAMPLES)
                {
                    ECGEMG_Settings.Current_ECG_Value = 0;
                }

                //play noise if enabled
                if(NoiseSettings.Noise_On == true)
                {
                    SPI_Transmit_DAC(DAC_Noise, noise_Samples[NoiseSettings.Current_Noise_Value]);
                    NoiseSettings.Current_Noise_Value++;

                    if(NoiseSettings.Current_Noise_Value > AMOUNT_NOISE_SAMPLES)
                    {
                        NoiseSettings.Current_Noise_Value = 0;
                    }
                }

                //play mains if enabled
                if(NoiseSettings.Mains_On == true)
                {
                    SPI_Transmit_DAC(DAC_50Hz, mains_Samples[NoiseSettings.Current_Mains_sample]);
                    NoiseSettings.Current_Mains_sample++;

                    if(NoiseSettings.Current_Mains_sample > AMOUNT_MAINS_SAMPLES)
                    {
                        NoiseSettings.Current_Mains_sample = 0;
                    }
                }

                timer = 0;
            }
            SysTick_enableInterrupt();
            break;

        case STATE_EMG:
            if(timer >=1)
            {
                //Play EMG signal
                SPI_Transmit_DAC(DAC_ECG_EMG, EMG_Samples[ECGEMG_Settings.Current_EMG_Value]);

                ECGEMG_Settings.Current_EMG_Value++;
                if(ECGEMG_Settings.Current_EMG_Value > AMOUNT_EMG_SAMPLES)
                {
                    ECGEMG_Settings.Current_EMG_Value = 0;
                }

                //play noise if enabled
                if(NoiseSettings.Noise_On == true)
                {
                    SPI_Transmit_DAC(DAC_Noise, noise_Samples[NoiseSettings.Current_Noise_Value]);
                    NoiseSettings.Current_Noise_Value++;

                    if(NoiseSettings.Current_Noise_Value > AMOUNT_NOISE_SAMPLES)
                    {
                        NoiseSettings.Current_Noise_Value = 0;
                    }
                }

                //play mains if enabled
                if(NoiseSettings.Mains_On == true)
                {
                    SPI_Transmit_DAC(DAC_50Hz, mains_Samples[NoiseSettings.Current_Mains_sample]);
                    NoiseSettings.Current_Mains_sample++;

                    if(NoiseSettings.Current_Mains_sample > AMOUNT_MAINS_SAMPLES)
                    {
                        NoiseSettings.Current_Mains_sample = 0;
                    }
                }

                timer = 0;
            }
            break;

        case STATE_BUTTONS:
            switch(Button_Pressed)
            {
            case BTN_AMP_PRESSED:
                Button_Pressed = BTN_NOT;
                //Increase amplitude of selected signal.
                break;
            case BTN_ECG_PRESSED:

                Button_Pressed = BTN_NOT;
                //switch to ECG if not already there. Otherwise increase heart rate.
                if(Previous_State != STATE_ECG)
                {
                    Previous_State = STATE_ECG;
                }else{
                    ECGEMG_Settings.Heartbeat_ECG++;
                    if(ECGEMG_Settings.Heartbeat_ECG >= 201)
                    {
                        ECGEMG_Settings.Heartbeat_ECG = 60;
                    }
                }
                // update display

                break;
            case BTN_EMG_PRESSED:

                Button_Pressed = BTN_NOT;
                //switch to EMG if not already there.
                if(Previous_State != STATE_EMG)
                {
                    Previous_State = STATE_EMG;
                }
                //update display

                break;
            case BTN_MAINS_PRESSED:

                Button_Pressed = BTN_NOT;
                //turn on/off 50Hz.
                NoiseSettings.Mains_On = !NoiseSettings.Mains_On;
                //update display

                break;
            case BTN_NOISE_PRESSED:

                Button_Pressed = BTN_NOT;
                //turn on/off noise.
                NoiseSettings.Noise_On = !NoiseSettings.Noise_On;
                //update display

                break;
            case BTN_SEL_PRESSED:
                Button_Pressed = BTN_NOT;
                //change selection.
                break;
            default:
                break;
            }

            //returns to the last state when no buttons have been pressed for a 1/3 of a second
            if(timer >= 171)
            {
                Current_State = Previous_State;
                timer = 0;
            }
            break;

        default:
            Current_State = STATE_SETUP;
            break;
        }
    }
}


void SysTick_Handler(void)
{
    timer++;
}

void PORT2_IRQHandler(void)
{
    Previous_State = Current_State;
    Current_State = STATE_BUTTONS;
    timer = 0;

    uint_fast16_t status;
    status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P2);

    switch(status)
    {
    case GPIO_PIN3:
        Button_Pressed = BTN_SEL_PRESSED;
        break;
    default:
        break;
    }

    GPIO_clearInterruptFlag(GPIO_PORT_P2, status);
    Interrupt_setPriorityMask(0);
}

void PORT3_IRQHandler(void)
{
    Previous_State = Current_State;
    Current_State = STATE_BUTTONS;
    timer = 0;

    uint_fast16_t status;
    status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P3);

    switch(status)
    {
    case GPIO_PIN7:
        Button_Pressed = BTN_EMG_PRESSED;
        break;
    case GPIO_PIN5:
        Button_Pressed = BTN_ECG_PRESSED;
        break;
    default:
        break;
    }

    GPIO_clearInterruptFlag(GPIO_PORT_P3, status);
    Interrupt_setPriorityMask(0);
}

void PORT4_IRQHandler(void)
{
    Previous_State = Current_State;
    Current_State = STATE_BUTTONS;
    timer = 0;

    uint_fast16_t status;
    status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P4);

    switch(status)
    {
    case GPIO_PIN1:
        Button_Pressed = BTN_NOISE_PRESSED;
        break;
    case GPIO_PIN6:
        Button_Pressed = BTN_MAINS_PRESSED;
        break;
    default:
        break;
    }

    GPIO_clearInterruptFlag(GPIO_PORT_P4, status);
    Interrupt_setPriorityMask(0);
}

void PORT5_IRQHandler(void)
{
    Previous_State = Current_State;
    Current_State = STATE_BUTTONS;
    timer = 0;

    uint_fast16_t status;
    status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P5);

    switch(status)
    {
    case GPIO_PIN1:
        Button_Pressed = BTN_AMP_PRESSED;
        break;
    default:
        break;
    }

    GPIO_clearInterruptFlag(GPIO_PORT_P5, status);
    Interrupt_setPriorityMask(0);
}
