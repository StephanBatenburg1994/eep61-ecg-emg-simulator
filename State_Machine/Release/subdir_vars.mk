################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../LCD.c \
../PotDacSPI.c \
../main.c \
../setup.c \
../system_msp432p401r.c 

C_DEPS += \
./LCD.d \
./PotDacSPI.d \
./main.d \
./setup.d \
./system_msp432p401r.d 

OBJS += \
./LCD.obj \
./PotDacSPI.obj \
./main.obj \
./setup.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"LCD.obj" \
"PotDacSPI.obj" \
"main.obj" \
"setup.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"LCD.d" \
"PotDacSPI.d" \
"main.d" \
"setup.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../LCD.c" \
"../PotDacSPI.c" \
"../main.c" \
"../setup.c" \
"../system_msp432p401r.c" 


