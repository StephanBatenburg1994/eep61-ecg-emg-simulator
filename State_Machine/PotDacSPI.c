/*
 * PotmeterSPI.c
 *
 *
 *
 *  CS1: DAC0  - P5.7   ---- DACECG input
 *  CS2: DAC1  - P2.1   ---- DACNoise input
 *  CS3: DAC2  - P5.2   ---- DAC50Hz input
 *  CS4: DP3   - P2.4   ---- noise regelen
 *  CS5: DP4   - P2.5   ---- 50 hz brom regelen
 *  CS6: DP1/2 - P2.7   ---- VrefRegelen DAC2/DAC1
 *  CS7: DP0   - P2.6   ---- VrefRegelen DAC 0
 *
 *  PW0 - PB0 Feedback
 *  PW1 - PB1 Spanningsdeler
 *
 *
 *                 MSP432P401
 *              -----------------
 *             |                 |
 *             |                 |
 *             |                 |
 *             |             P1.6|-> Data Out (UCB0SIMO)
 *             |                 |
 *             |             P1.7|<- Data In (UCB0SOMI)
 *             |                 |
 *             |             P1.5|-> Serial Clock Out (UCB0CLK)
 *
 *
 *
 *
 *  Created on: 16 mei 2018
 *      Author: Cas
 */

#include <stdint.h>
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>
#include "PotDacSPI.h"


void initSPI(void)
{

    /* Configuring SPI in 3wire master mode */
    const eUSCI_SPI_MasterConfig spiMasterConfig =
    {
            EUSCI_B_SPI_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
            6000000,                                   // SMCLK = 1 MHZ
            3000000,                                    // SPICLK = 1 Mhz
            EUSCI_B_SPI_MSB_FIRST,                     // MSB First
            EUSCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,    // Phase
            EUSCI_B_SPI_CLOCKPOLARITY_INACTIVITY_HIGH, // High polarity
            EUSCI_B_SPI_3PIN                          // 3Wire SPI Mode
    };
    SPI_initMaster(EUSCI_B0_BASE, &spiMasterConfig);

    /* Selecting P1.5 P1.6 in SPI mode */
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
            GPIO_PIN5 | GPIO_PIN6, GPIO_PRIMARY_MODULE_FUNCTION);

    //CS For DACS and POTS
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN2 | GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN1 | GPIO_PIN4 | GPIO_PIN5 | GPIO_PIN6 | GPIO_PIN7);


    //All Chip selects HIGH
    GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN2 | GPIO_PIN7);
    GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN1 | GPIO_PIN4 | GPIO_PIN5 | GPIO_PIN6 | GPIO_PIN7);

    /* Enable SPI module */
    SPI_enableModule(EUSCI_B0_BASE);
}

uint8_t VppPOT_conversion(signal_T signal, float voltage)
{
    float potVal;
    if(signal == ECG)
    {
        potVal = voltage / 0.0121;
        return (uint8_t)potVal;
    }
    if(signal == EMG)
    {
        potVal = voltage / 0.0121;
        return (uint8_t)potVal;
    }
    else
    {
        return -1;
    }
}

void SPI_Transmit_Pot(uint8_t Potname, uint8_t value)
{
    uint8_t Port;
    uint16_t Pin;
    uint16_t TXData;

    /* Config settings */
    switch(Potname)
    {
    case 0:                         //CS7: DP0   - P2.6   ---- Regelen DAC 0
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN6;
        TXData = 0x1100;
        break;
    case 1:                         //CS6: DP1   - P2.7   ---- Regelen DAC2
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN7;
        TXData = 0x1200;
        break;
    case 2:                         //CS6: DP2   - P2.7   ---- Regelen DAC1
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN7;
        TXData = 0x1100;
        break;
    case 3:                         //CS5: DP4   - P2.5   ---- 50 hz brom feedback
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN5;
        TXData = 0x1100;
        break;
    case 4:                         //CS5: DP4   - P2.5   ---- 50 hz brom spanningsdeler
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN5;
        TXData = 0x1200;
        break;
    case 5:                         //CS4: DP3   - P2.4   ---- noise feedback
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN4;
        TXData = 0x1100;
        break;
    case 6:                         //CS4: DP3   - P2.4   ---- noise spanningsdeler
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN4;
        TXData = 0x1200;
        break;
    }

    // Add the transmit value to the settings
    TXData |= value;
    /* Polling to see if the TX buffer is ready */
    while (!(SPI_getInterruptStatus(EUSCI_B0_BASE,EUSCI_B_SPI_TRANSMIT_INTERRUPT)));
    /* Transmitting data to slave */
    GPIO_setOutputLowOnPin(Port, Pin);
      SPI_transmitData(EUSCI_B0_BASE, TXData>>8);
      SPI_transmitData(EUSCI_B0_BASE, TXData);
    GPIO_setOutputHighOnPin(Port, Pin);

}

void SPI_Transmit_DAC(uint8_t DACnumber, uint16_t value)
{
    uint8_t Port;
    uint16_t Pin;
    uint16_t TXData = 0x3000 | value;

    switch(DACnumber)
    {
    case 0:                         //CS1: DAC0  - P5.7   ---- DACECGEMG output
        Port = GPIO_PORT_P5;
        Pin = GPIO_PIN7;
        break;
    case 1:                         //CS2: DAC1  - P2.1   ---- DACNoise output
        Port = GPIO_PORT_P2;
        Pin = GPIO_PIN1;
        break;
    case 2:                         //CS3: DAC2  - P5.2   ---- DAC50Hz output
        Port = GPIO_PORT_P5;
        Pin = GPIO_PIN2;
        break;
    }
    int i;
    //Fire and Forget
    while (!(SPI_getInterruptStatus(EUSCI_B0_BASE,EUSCI_B_SPI_TRANSMIT_INTERRUPT)));
    GPIO_setOutputLowOnPin(Port, Pin);
    for(i = 20000; i > 0;i--);
      //SPI_transmitData(EUSCI_B0_BASE, TXData>>8);
      //SPI_transmitData(EUSCI_B0_BASE, TXData);
     // for(i = 20; i > 0;i--);
    GPIO_setOutputHighOnPin(Port, Pin);
}

