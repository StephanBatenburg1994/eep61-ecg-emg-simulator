/*
 *  File: setup.c
 *  Author @ Stephan Batenburg
 *  Date: May 21, 2018
 *
 *  Description:
 *  This library is used for setting up the variables that are used for the ECG/EMG and noise outputs.
 */

#include <setup.h>

void output_Set_Default_Settings(noise_Settings * NoiseSettings, ECG_EMG_Settings * ECGEMGSettings)
{
    NoiseSettings->Mains_Amplitude = 1023;
    NoiseSettings->Mains_On = true;
    NoiseSettings->Noise_On = true;
    NoiseSettings->Noise_Amplitude = 127;
    NoiseSettings->Select_Mains_SignalType = MAINS_SINE;
    NoiseSettings->Current_Mains_sample = 0;
    NoiseSettings->Current_Noise_Value = 0;

    ECGEMGSettings->Select_EMG_ECG = ECG_SIGNAL;
    ECGEMGSettings->Amplitude_ECG = 1023;
    ECGEMGSettings->Amplitude_EMG = 1023;
    ECGEMGSettings->Heartbeat_ECG = 80;
    ECGEMGSettings->Current_ECG_Value = 0;
    ECGEMGSettings->Current_EMG_Value = 0;
}

void set_Timers(void)
{
    //This has been taken from the example:
    //http://dev.ti.com/tirex/#/?link=Software%2FSimpleLink%20MSP432P4%20SDK%2FExamples%2FDevelopment%20Tools%2FMSP432P401R%20LaunchPad%20-%20Red%202.x%20(Red)%2FDriverLib%2Fcs_hfxt_start%2FNo%20RTOS%2FCCS%20Compiler%2Fcs_hfxt_start%2Fcs_hfxt_start.c

    /* Setting the external clock frequency. This API is optional, but will
     * come in handy if the user ever wants to use the getMCLK/getACLK/etc
     * functions
     */
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_PJ,
            GPIO_PIN3 | GPIO_PIN2, GPIO_PRIMARY_MODULE_FUNCTION);
    CS_setExternalClockSourceFrequency(SEC_CLOCKSPEED,MAIN_CLOCKSPEED);

    /* Starting HFXT in non-bypass mode without a timeout. Before we start
     * we have to change VCORE to 1 to support the 48MHz frequency */
    PCM_setCoreVoltageLevel(PCM_VCORE1);
    FlashCtl_setWaitState(FLASH_BANK0, 2);
    FlashCtl_setWaitState(FLASH_BANK1, 2);
    CS_startHFXT(false);

    /* Initializing MCLK to HFXT (effectively 48MHz) */
    CS_initClockSignal(CS_MCLK, CS_HFXTCLK_SELECT, CS_CLOCK_DIVIDER_8);

    SysTick_enableModule();
    // 1/512 of a second:
    SysTick_setPeriod(93750);
    SysTick_enableInterrupt();

    Interrupt_enableMaster();
}

void Setup_PinInterrupts()
{
    /*
     *  Used buttons are:
     * P3.7 BTN5 EMG
     * P3.5 BTN4 ECG/BPM
     * P4.1 BTN3 NOISE
     * P4.6 BTN2 50Hz (mains)
     * P5.1 BTN1 Amplitude
     * P2.3 BTN0 Select
     */

     GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P3, GPIO_PIN7);
     GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P3, GPIO_PIN5);
     GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P4, GPIO_PIN1);
     GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P4, GPIO_PIN6);
     GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P5, GPIO_PIN1);
     GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P2, GPIO_PIN3);

     GPIO_clearInterruptFlag(GPIO_PORT_P3, GPIO_PIN7);
     GPIO_clearInterruptFlag(GPIO_PORT_P3, GPIO_PIN5);
     GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN1);
     GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN6);
     GPIO_clearInterruptFlag(GPIO_PORT_P5, GPIO_PIN1);
     GPIO_clearInterruptFlag(GPIO_PORT_P2, GPIO_PIN3);

     Interrupt_setPriority(INT_PORT2, 0x20);
     Interrupt_setPriority(INT_PORT3, 0x21);
     Interrupt_setPriority(INT_PORT4, 0x22);
     Interrupt_setPriority(INT_PORT5, 0x23);

     Interrupt_setPriorityMask(0x40);

     GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN7);
     GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN5);
     GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN1);
     GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN6);
     GPIO_enableInterrupt(GPIO_PORT_P5, GPIO_PIN1);
     GPIO_enableInterrupt(GPIO_PORT_P2, GPIO_PIN3);

     Interrupt_enableInterrupt(INT_PORT2);
     Interrupt_enableInterrupt(INT_PORT3);
     Interrupt_enableInterrupt(INT_PORT4);
     Interrupt_enableInterrupt(INT_PORT5);
}
