/*
 * File: states.h
 * @ Author: Stephan Batenburg
 * Date: may 30, 2018
 *
 * Description:
 * This file is used for defining the states where the device can be in
 */

#ifndef __STATES__


//the amount of samples -1
#define AMOUNT_EMG_SAMPLES 511
#define AMOUNT_ECG_SAMPLES 511
#define AMOUNT_NOISE_SAMPLES 511
#define AMOUNT_MAINS_SAMPLES 511

uint8_t Previous_State = 0x01;
uint8_t Current_State = 0x00;
uint8_t Button_Pressed = 0x00;
uint8_t Current_Selection = 0x00;

//defines for states
#define STATE_SETUP 0x00
#define STATE_ECG 0x01
#define STATE_EMG 0x02
#define STATE_BUTTONS 0x03

//defines for buttons
#define BTN_NOT 0x00
#define BTN_SEL_PRESSED 0x01
#define BTN_AMP_PRESSED 0x02
#define BTN_MAINS_PRESSED 0x03
#define BTN_NOISE_PRESSED 0x04
#define BTN_ECG_PRESSED 0x05
#define BTN_EMG_PRESSED 0x06

#endif // __STATES__
